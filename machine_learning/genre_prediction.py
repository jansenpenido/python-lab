from sklearn import tree

# [height, weight, shoe size]
measurements = [[181,80,44], [177,70,43], [160,60,38], [154,54,37],
				[166,65,40], [190,90,47], [175,64,39], [177,70,40], 
				[159,55,37], [171,75,42], [181,85,43]]

genre = ['male', 'female', 'female', 'female', 
		 'male', 'male', 'male', 'female', 
		 'male', 'female', 'male']

# Create decision tree
clf = tree.DecisionTreeClassifier()
clf = clf.fit(measurements, genre)

# Prediction test
prediction = clf.predict([[160,70,33]])
print(prediction)

