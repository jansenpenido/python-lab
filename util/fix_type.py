from datetime import datetime as dt

def test():
	print('Hello!')

# Takes a date as a string, and returns a Python datetime object. 
# If there is no date given, returns None
def parse_date(date):
	if date == '':
		return None
	else:
		return dt.strptime(date, '%Y-%m-%d')
	
# Takes a string which is either an empty string or represents an integer,
# and returns an int or None.
def parse_maybe_int(i):
	if i == '':
		return None
	else:
		return int(i)
	
	# Takes a boolean as a string and returns the corresponding Boolean value.
	def parse_bool(b):
		return b.lower() == 'true'
