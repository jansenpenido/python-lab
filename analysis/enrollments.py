import unicodecsv

# So, here are the file paths
enrollments_filename = './data/enrollments.csv'
daily_engagement_file = './data/daily_engagement.csv'
project_submissions_file = './data/project_submissions.csv'

# Let's read the files
with open(enrollments_filename, 'rb') as f:
    reader = unicodecsv.DictReader(f)
    enrollments = list(reader)

with open(daily_engagement_file, 'rb') as f:
	reader = unicodecsv.DictReader(f)
	daily_engagement = list(reader)

with open(project_submissions_file, 'rb') as f:
	reader = unicodecsv.DictReader(f)
	project_submissions = list(reader)

# Finally, let there be text!
print( len(enrollments) )
print( len(daily_engagement) )
print( len(project_submissions) )
