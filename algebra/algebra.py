import math

class Vector(object):
	def __init__(self, coordinates):
		try:
			if not coordinates:
				raise ValueError
			self.coordinates = tuple(coordinates)
			self.dimension = len(coordinates)
			
		except ValueError:
			raise ValueError('The coordinates must be nonempty')
		
		except TypeError:
			raise TypeError('The coordinates must be an iterable')
			
	def __str__(self):
		return 'Vector: {}'.format(self.coordinates)
		
	def __eq__(self, v):
		return self.coordinates == v.coordinates
        
	def __add__(self, v):
		new_coordinates = [x+y for x,y in zip(self.coordinates, v.coordinates)]
		return Vector(new_coordinates)
	
	def __sub__(self, v):
		new_coordinates = [x-y for x,y in zip(self.coordinates, v.coordinates)]
		return Vector(new_coordinates)        
	
	def times_scalar(self, c):
		new_coordinates = [x*c for x in self.coordinates]
		return Vector(new_coordinates)
	
	def magnitude(self):
		coordinates_squared = sum([math.pow(x,2) for x in self.coordinates])
		return math.sqrt(coordinates_squared)
	
	def normalize(self):
		try:
			return self.times_scalar(1/self.magnitude())
		except ZeroDivisionError:
			raise Exception('Cannot normalize the zero vector')
		
	def inner_product(self, v):
		product_vector = [x*y for x,y in zip(self.coordinates, v.coordinates)]
		return sum(product_vector)
	
	def angle_rad(self, v):
		try:
			return math.acos( self.inner_product(v) / ( self.magnitude() * v.magnitude() ) )
		except ZeroDivisionError:
			raise Exception('Cannot calculate angle for zero vector')
		
	def angle_degrees(self, v):
		angle_rad = self.angle_rad(v)
		return math.degrees(angle_rad)
	
